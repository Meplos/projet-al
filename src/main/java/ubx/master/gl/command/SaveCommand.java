package ubx.master.gl.command;

import ubx.master.gl.board.Savable;

public class SaveCommand implements Command {
    Savable savable;
    String pathToSave;

    public void Execute(){
        System.out.println("Execute");
        savable.save(pathToSave);
    }

    public void undo() {
        throw new RuntimeException("Cannot reverse save command");
    }


    public SaveCommand(Savable savable, String pathToSave) {
        this.savable = savable;
        this.pathToSave = pathToSave;
    }

    
    
}