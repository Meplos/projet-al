package ubx.master.gl.command;

import java.util.Stack;

public class CommandQueue {
    private Stack<Command> undoQueue;
    private Stack<Command> redoQueue;

    public CommandQueue(){
        undoQueue = new Stack<Command>();
        redoQueue = new Stack<Command>();
    }

    public void undo() {
        if(undoQueue.empty()) return;
        Command c = undoQueue.pop();
        insertRedoQueue(c);
        c.undo();
    }

    public void redo() {
        if(redoQueue.empty()) return;
        Command c = redoQueue.pop();
        insertUndoQueue(c);
        c.Execute();

    }

    public void insertUndoQueue(Command c) {
        this.undoQueue.addElement(c);
    }

    private void insertRedoQueue(Command c) {
        redoQueue.add(c);
    }


}