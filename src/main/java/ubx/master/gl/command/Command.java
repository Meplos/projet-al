package ubx.master.gl.command;

public interface Command {

    public void Execute();
    public void undo();
}