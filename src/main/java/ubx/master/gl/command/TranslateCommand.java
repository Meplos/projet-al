package ubx.master.gl.command;

import ubx.master.gl.shape.Shape;


public class TranslateCommand implements Command {
    private double transX, transY;
    private Shape shape;

    public TranslateCommand(Shape shape,double transX,double transY) {
        this.shape = shape;
        this.transX = transX;
        this.transY = transY;

    }
	@Override
	public void Execute() {
		this.shape.translate(transX, transY);
		
	}

	@Override
	public void undo() {
		this.shape.translate(-transX, -transY);
		
	}

    

}