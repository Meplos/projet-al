package ubx.master.gl.command;


import ubx.master.gl.shape.RegularPolygone;

public class NbSideCommand implements Command {

    private RegularPolygone shape;

    private double oldNbSide;

    private double newNbSide;

    public NbSideCommand(RegularPolygone regpoly, double newNbSide) {
        this.shape = regpoly;
        this.oldNbSide = this.shape.getNbSide();
        this.newNbSide = newNbSide;
    }

    @Override
    public void Execute() {
        shape.setNbSide(newNbSide);

    }

    @Override
    public void undo() {
        shape.setNbSide(oldNbSide);
    }

}