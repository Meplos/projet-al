package ubx.master.gl.command;


import ubx.master.gl.shape.Shape;

public class RotateCommand implements Command {
    private double angle;
    private Shape shape;

    

    public void Execute() {
        shape.rotate(angle);
    }

    public void undo() {
        shape.rotate(-angle);
    }



	public RotateCommand(Shape s, double angle) {
        this.shape = s;
        this.angle = angle;
	}


}