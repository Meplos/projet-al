package ubx.master.gl.command;

import ubx.master.gl.shape.Shape;
import ubx.master.gl.board.Whiteboard;

public class AddCommand implements Command {
   private Whiteboard whiteboard;
    private Shape shapeToAdd;

    public AddCommand(Whiteboard whiteboard, Shape shape){
        this.whiteboard = whiteboard;
        this.shapeToAdd = shape;
    }

	@Override
	public void Execute() {
		this.whiteboard.add(shapeToAdd);
		
	}

	@Override
	public void undo() {
		this.whiteboard.remove(shapeToAdd);
		
	}
}