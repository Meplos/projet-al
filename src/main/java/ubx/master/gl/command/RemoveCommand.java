package ubx.master.gl.command;

import ubx.master.gl.board.Savable;

import ubx.master.gl.shape.Shape;

public class RemoveCommand implements Command {
    private Savable savable;
    private Shape shapeToRemove;

    public RemoveCommand(Savable savable, Shape shape){
        this.savable = savable;
        this.shapeToRemove = shape;
    }

	@Override
	public void Execute() {
		this.savable.remove(shapeToRemove);
		
	}

	@Override
	public void undo() {
		this.savable.add(shapeToRemove);
		
	}


}