package ubx.master.gl.command;

import ubx.master.gl.shape.*;
public class HeightCommand implements Command {

    private double oldHeight;
    private double newHeight;
    private Rectangle shape;

    public HeightCommand(Rectangle rect, double newHeight) {
        this.shape = rect;
        this.newHeight = newHeight;
        this.oldHeight = this.shape.getHeight();
    }

    @Override
    public void Execute() {
        shape.changeHeight(newHeight);

    }

    @Override
    public void undo() {
        shape.changeHeight(oldHeight);
    }

}