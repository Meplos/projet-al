package ubx.master.gl.command;

import ubx.master.gl.shape.Rectangle;

public class ArcCommand implements Command {

    private Rectangle shape;
    private  double oldRounded;   
    private double newRounded;

    public ArcCommand(Rectangle rect, double newRounded) {
        this.shape = rect;
        this.oldRounded = this.shape.getRounded();
        this.newRounded = newRounded;
    }
    
    @Override
    public void Execute() {
        shape.setRounded(newRounded);

    }

    @Override
    public void undo() {
        shape.setRounded(oldRounded);
    }
    
}