package ubx.master.gl.command;

import ubx.master.gl.shape.*;

public class ColorCommand implements Command{
    private Shape s;
    private Color oldColor;
    private Color newColor;

    public ColorCommand(Shape s, Color newColor) {
        this.s = s;
        this.oldColor = ((AbstractShape) this.s).getColor();
        this.newColor = newColor;
    }

	@Override
	public void Execute() {
        
		this.s.setColor(newColor);
		
	}

	@Override
	public void undo() {
		this.s.setColor(oldColor);
		
	}

}