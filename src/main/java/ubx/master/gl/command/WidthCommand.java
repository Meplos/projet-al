package ubx.master.gl.command;

import ubx.master.gl.shape.*;

public class WidthCommand implements Command{

    private double oldWidth;
    private double newWidth;
    private Rectangle shape;
    
    public WidthCommand(Rectangle rect, double newWidth){
        this.shape = rect;
        this.newWidth = newWidth;
        this.oldWidth = this.shape.getWidth();
    }
    
    @Override
    public void Execute() {
        shape.changeWidth(newWidth);

    }

    @Override
    public void undo() {
        shape.changeWidth(oldWidth);
    }
    
}