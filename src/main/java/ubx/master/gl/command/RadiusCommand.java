package ubx.master.gl.command;


import ubx.master.gl.shape.RegularPolygone;

public class RadiusCommand implements Command{

    private RegularPolygone shape;
    
    private double oldRad;

    private double newRad;

    public RadiusCommand(RegularPolygone regpoly, double newRad){
        this.shape = regpoly;
        this.oldRad = this.shape.getRadius();
        this.newRad = newRad;
    }

    @Override
    public void Execute() {
      shape.changeRadius(newRad);

    }

    @Override
    public void undo() {
        shape.changeRadius(oldRad);
    }



}