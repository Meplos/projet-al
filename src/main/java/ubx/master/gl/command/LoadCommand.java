package ubx.master.gl.command;

import ubx.master.gl.board.Savable;

public class LoadCommand implements Command {
    private Savable savable;
    private String pathToLoad;

    public void Execute() {
        System.out.println("Execute");
        savable.load(pathToLoad);
    }

    public void undo() {
        throw new RuntimeException("Cannot reverse save command");
    }

    public LoadCommand(Savable savable, String pathToLoad) {
        this.savable = savable;
        this.pathToLoad = pathToLoad;
    }

}