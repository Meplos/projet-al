package ubx.master.gl.json;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

import ubx.master.gl.shape.Point2D;
import ubx.master.gl.shape.Color;
import ubx.master.gl.shape.Group;
import ubx.master.gl.shape.Rectangle;
import ubx.master.gl.shape.RegularPolygone;
import ubx.master.gl.shape.Shape;

public class JsonManager {
 public JsonManager(){

 }


 public List<Shape> extractShape(String path) {
     JSONParser jsonParser = new JSONParser();
     try (FileReader reader = new FileReader(path))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
 
            JSONArray shapeList = (JSONArray) obj;
            System.out.println(shapeList);
             
            //Iterate over employee array
            List<Shape> shapes = new ArrayList<>();
            shapeList.forEach( sh -> shapes.add(parseShapeObject((JSONObject) sh )));
 
            return shapes;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    
     return new ArrayList<Shape>();
 }

 private Shape parseShapeObject(JSONObject shape) {
    Shape s = null;
    String type = (String) shape.get("type");
    switch(type){
        case "rectangle":
                s = parseRectangle(shape);
            break;
        case "regularPolygone":
            s  = parseRegularPolygone(shape);
            break;
        case "group":
            s = parseGroup((JSONArray) shape.get("shapes"));
            break;
        default:
            throw new RuntimeException("Illegal format. Type : "+ type +" is unknown");
    }
    return s;
     
 }
 public Shape parseGroup(JSONArray obj){
    Group group = new Group();
    obj.forEach(sh -> group.addShape(parseShapeObject((JSONObject)sh)));
    
    return group;
 }

 private Shape parseRectangle( JSONObject obj) {
    JSONObject jsonPosition = (JSONObject) obj.get("position");
    Point2D position = new Point2D((double) jsonPosition.get("x"),(double) jsonPosition.get("y"));
    JSONObject jsonRotationCenter = (JSONObject) obj.get("rotationCenter");
    Point2D rotationCenter = new Point2D((double) jsonRotationCenter.get("x"),(double) jsonRotationCenter.get("y"));
    JSONObject jsonColor = (JSONObject) obj.get("color");
    Color color = new Color(((Long) jsonColor.get("r")).intValue(), ((Long) jsonColor.get("v")).intValue(), ((Long) jsonColor.get("b")).intValue());
    double width = (double) obj.get("width"); 
    double height = (double) obj.get("height");
    double rounded = (double) obj.get("rounded");
    double angle = (double) obj.get("angle");
    JSONArray jsonPoints = (JSONArray) obj.get("points");
    List<Point2D> points = new ArrayList<>();
    for(int i = 0; i < jsonPoints.size(); i++){
        JSONObject curr = (JSONObject) jsonPoints.get(i);
        points.add(new Point2D((double) curr.get("x"),(double) curr.get("y")));
    }
    return new Rectangle(position, rotationCenter, color, width, height, rounded,points, angle);
 }

 private Shape parseRegularPolygone(JSONObject obj) {
    JSONObject jsonRotationCenter = (JSONObject) obj.get("rotationCenter");
    Point2D rotationCenter = new Point2D((double) jsonRotationCenter.get("x"),(double) jsonRotationCenter.get("y"));
    JSONObject jsonColor = (JSONObject) obj.get("color");
    Color color = new Color(((Long) jsonColor.get("r")).intValue(), ((Long) jsonColor.get("v")).intValue(), ((Long) jsonColor.get("b")).intValue());
    double nbSide = (double) obj.get("nbSide"); 
    double radius = (double) obj.get("radius");
    double angle = (double) obj.get("angle");
    JSONArray jsonPoints = (JSONArray) obj.get("points");
    List<Point2D> points = new ArrayList<>();
    for(int i = 0; i < jsonPoints.size(); i++){
        JSONObject curr = (JSONObject) jsonPoints.get(i);
        points.add(new Point2D((double) curr.get("x"),(double) curr.get("y")));
    }
    return new RegularPolygone(rotationCenter, color, nbSide, radius,points,angle);
 }
}