package ubx.master.gl.api;

import java.util.List;

import ubx.master.gl.shape.Color;
import ubx.master.gl.shape.Point2D;

public interface DrawApi {
    public void draw(FrameApi frame);
    public void drawThumbnail(FrameApi frame);
    public void setPoint2D(List<Point2D> pts);
    public void setArc(double arc);
    public void setColor(Color color);
    public void setAngle(double angle);
	public void setSelection(Boolean select);
    
}