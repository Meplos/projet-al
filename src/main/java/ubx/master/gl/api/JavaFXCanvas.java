package ubx.master.gl.api;

import javafx.scene.canvas.Canvas;

public class JavaFXCanvas extends Canvas implements FrameApi {

    public JavaFXCanvas(double DIMX, double DIMY){
		super(DIMX,DIMY);
		
	}

	@Override
	public FrameApi getFrame() {
		return this;
	}

}