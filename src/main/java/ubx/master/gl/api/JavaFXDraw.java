package ubx.master.gl.api;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Rotate;
import ubx.master.gl.builder.JavaFXBuilder;
import ubx.master.gl.shape.Color;
import ubx.master.gl.shape.Point2D;

public class JavaFXDraw implements DrawApi  {
        private final double RESIZE = .25;
        private List<Point2D> pts;
        private double arc;
        private Color color;
        private double angle;
        private Point2D rotationCenter;
        private boolean isSelect;

        public JavaFXDraw(List<Point2D> pts, Point2D rotationCenter, double arc, double angle, Color color, boolean select) {
                this.pts = pts;
                this.arc = arc;
                this.color = color;
                this.rotationCenter = rotationCenter;
                this.angle = angle;
                this.isSelect = select;
                
        }

        @Override
        public void draw(FrameApi frame) {
                if (!(frame instanceof JavaFXCanvas))
                        throw new RuntimeException("Illegal argument!");
                if (arc > 0 && pts.size() > 4)
                        throw new RuntimeException("Only rectangle can have round border");
                
                if (arc > 0)
                        drawRoundRect(frame);
                else
                        drawRegularShape(frame);

                // gc.fillRect(10,10,20, 20);

        }

        @Override
        public void drawThumbnail(FrameApi frame){
                List<Point2D> thumbPoint = new ArrayList<>();
                for (int i = 0; i < this.pts.size(); i++) {
                        thumbPoint.add(replaceThumbnailPoint(i));
                }
                List<Point2D> tmp = this.pts;
                this.pts = thumbPoint;
                System.out.println("Thumbpoint : !!!!!!!!!");
                System.out.println(thumbPoint.get(0).toJSON());
                draw(frame);
                this.pts = tmp;
        }

        private Point2D replaceThumbnailPoint(int i){
                double newX =  RESIZE*(this.pts.get(i).getX() - this.rotationCenter.getX()) + (JavaFXBuilder.toolSize/2);
                double newY =  RESIZE*(this.pts.get(i).getY() - this.rotationCenter.getY()) + (JavaFXBuilder.toolSize/2);
                return new Point2D(newX,newY);
        }
        private void drawRegularShape(FrameApi frame) {
                System.out.println("Regular shape");
                System.out.println(frame);

                int nbpoint = pts.size();
                JavaFXCanvas canvas = (JavaFXCanvas) frame;
                GraphicsContext gc = canvas.getGraphicsContext2D();
                gc.setFill(javafx.scene.paint.Color.rgb(color.getR(), color.getV(), color.getB()));
                gc.beginPath();
                if(pts.get(0).getX() < JavaFXBuilder.toolSize){
                        gc.moveTo(pts.get(0).getX(), pts.get(0).getY());
                        for (int i = 0; i < nbpoint; i++) {
                                gc.lineTo(pts.get((i + 1) % nbpoint).getX(), pts.get((i + 1) % nbpoint).getY());
                        }
                        gc.closePath();
                        gc.fill();

                        if(isSelect) {
                                gc.setStroke(javafx.scene.paint.Color.BLACK);
                                gc.setLineWidth(5);
                                gc.beginPath();
                                gc.moveTo(pts.get(0).getX(), pts.get(0).getY());
                                for (int i = 0; i < nbpoint; i++) {
                                        gc.lineTo(pts.get((i + 1) % nbpoint).getX(), pts.get((i + 1) % nbpoint).getY());
                                }
                                gc.closePath();
                                gc.stroke();
                        }
                }else{
                        gc.moveTo(pts.get(0).getX() - JavaFXBuilder.toolSize, pts.get(0).getY() - JavaFXBuilder.menuSize);
                        for (int i = 0; i < nbpoint; i++) {
                                gc.lineTo(pts.get((i + 1) % nbpoint).getX() - JavaFXBuilder.toolSize, pts.get((i + 1) % nbpoint).getY() - JavaFXBuilder.menuSize);
                        }
                        gc.closePath();
                        gc.fill();

                        if(isSelect) {
                                gc.setStroke(javafx.scene.paint.Color.BLACK);
                                gc.setLineWidth(5);
                                gc.beginPath();
                                gc.moveTo(pts.get(0).getX(), pts.get(0).getY());
                                for (int i = 0; i < nbpoint; i++) {
                                        gc.lineTo(pts.get((i + 1) % nbpoint).getX(), pts.get((i + 1) % nbpoint).getY());
                                }
                                gc.closePath();
                                gc.stroke();
                        }
                }
        }

        private void drawRoundRect(FrameApi frame) {
                System.out.println("Round Rect");
                GraphicsContext gc = ((JavaFXCanvas) frame).getGraphicsContext2D();
                gc.save();
                gc.setFill(javafx.scene.paint.Color.rgb(color.getR(), color.getV(), color.getB()));
                gc.transform(new Affine(new Rotate(this.angle,this.rotationCenter.getX(),this.rotationCenter.getY())));

                double width = Math.sqrt(Math.pow(pts.get(0).getX() - pts.get(1).getX(), 2)
                                + Math.pow(pts.get(0).getY() - pts.get(1).getY(), 2));
                double height = Math.sqrt(Math.pow(pts.get(0).getX() - pts.get(3).getX(), 2)
                                + Math.pow(pts.get(0).getY() - pts.get(3).getY(), 2));

                if(pts.get(0).getX() < JavaFXBuilder.toolSize){
                        gc.fillRoundRect(pts.get(0).getX(), pts.get(0).getY(), width, height, arc, arc);
                        gc.restore();
                        if(isSelect){
                                gc.save();
                                gc.setStroke(javafx.scene.paint.Color.BLACK);
                                gc.setLineWidth(20);
                                gc.transform(new Affine(new Rotate(this.angle, this.rotationCenter.getX(),
                                                this.rotationCenter.getY())));

                                gc.strokeRoundRect(pts.get(0).getX(), pts.get(0).getY(), width, height, arc, arc);
                                gc.restore();
                        }
                }else{
                        gc.fillRoundRect(pts.get(0).getX() - JavaFXBuilder.toolSize , pts.get(0).getY() - JavaFXBuilder.menuSize, width, height, arc, arc);
                        gc.restore();
                        if(isSelect){
                                gc.save();
                                gc.setStroke(javafx.scene.paint.Color.BLACK);
                                gc.setLineWidth(20);
                                gc.transform(new Affine(new Rotate(this.angle, this.rotationCenter.getX(),
                                                this.rotationCenter.getY())));

                                gc.strokeRoundRect(pts.get(0).getX(), pts.get(0).getY(), width, height, arc, arc);
                                gc.restore();
                        }
                }
        }

        @Override
        public void setPoint2D(List<Point2D> pts) {
                this.pts = pts;
                System.out.println("DRAWAPI : ");
                for (Point2D point2d : pts) {

                        System.out.println(point2d.getX() + " " + point2d.getY());
                }
        }

        @Override
        public void setArc(double arc) {
                this.arc = arc;
        }

        @Override
        public void setColor(Color color) {
                this.color = color;
        }

        @Override
        public void setAngle(double angle) {
                this.angle = angle;
                System.err.println("DRAWTATE");

        }

        @Override
        public void setSelection(Boolean select) {
                this.isSelect = select;
                System.out.println("setSelection : " + select);
        }
}