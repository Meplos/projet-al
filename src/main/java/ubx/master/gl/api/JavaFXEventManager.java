package ubx.master.gl.api;

import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import ubx.master.gl.board.ToolBar;
import ubx.master.gl.board.Whiteboard;
import ubx.master.gl.builder.JavaFXBuilder;
import ubx.master.gl.command.*;
import ubx.master.gl.shape.Group;
import ubx.master.gl.shape.Point2D;
import ubx.master.gl.shape.Rectangle;
import ubx.master.gl.shape.RegularPolygone;
import ubx.master.gl.shape.Shape;

public class JavaFXEventManager {
    private static JavaFXEventManager instance = null;
    private Scene scene;
    private Whiteboard whiteboard;
    private ToolBar toolbar;
    private CommandQueue queue;

    private Shape select;
    private double mouseClickX, mouseClickY;
    private double minX = 0, minY = 0, maxX = 0, maxY = 0;

    private boolean fromBoard = false;

    private JavaFXEventManager(Scene s, Whiteboard wh, ToolBar tools) {
        scene = s;
        whiteboard = wh;
        toolbar = tools;
        queue = new CommandQueue();
    }

    public static JavaFXEventManager getInstance(Scene s, Whiteboard wh, ToolBar tools) {
        if (instance == null) {
            instance = new JavaFXEventManager(s, wh, tools);
        }
        return instance;
    }

    public void initEvent() {

        EventHandler<MouseEvent> pressEvent = new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.SECONDARY) {
                        Shape mod = whiteboard.getShapeByPos(event.getSceneX(), event.getSceneY());
                        if(mod != null){
                            System.out.println("Toucher !!!");
                            if(mod instanceof Rectangle){
                                // Create ContextMenu
                                ContextMenu contextMenu = new ContextMenu();
                                MenuItem item1 = new MenuItem("Changer la longueur");
                                MenuItem item2 = new MenuItem("Changer la largeur");
                                MenuItem item5 = new MenuItem("Changer l'arrondissement des angle");
                                item1.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        TextInputDialog dialog = new TextInputDialog("Nouvelle longueur");
                                        dialog.setTitle("Changement longueur");
                                        dialog.setHeaderText("Indiquez la nouvelle longueur");
                                        Optional<String> result = dialog.showAndWait();
                                        if (result.isPresent()){
                                            System.out.println("Longueur: " + result.get());
                                            double newWidth = Double.parseDouble(result.get());
                                            Rectangle modif = (Rectangle) (mod);
                                            System.out.println(mod);
                                            System.out.println(modif);
                                            //modif.changeWidth(newWidth);
                                            Command c = new WidthCommand(modif,newWidth);
                                            c.Execute();
                                            queue.insertUndoQueue(c);
                                            
                                        }
                                    }
                                });
                                item2.setOnAction(new EventHandler<ActionEvent>() {
 
                                    @Override
                                    public void handle(ActionEvent event) {
                                        TextInputDialog dialog = new TextInputDialog("Nouvelle largeur");
                                        dialog.setTitle("Changement largeur");
                                        dialog.setHeaderText("Indiquez la nouvelle largeur");
                                        Optional<String> result = dialog.showAndWait();
                                        if (result.isPresent()){
                                            System.out.println("Largeur: " + result.get());
                                            double newHeight = Double.parseDouble(result.get());
                                            Rectangle modif = (Rectangle) (mod);
                                            //modif.changeHeight(newHeight);
                                            Command c = new HeightCommand(modif,newHeight);
                                            c.Execute();
                                            queue.insertUndoQueue(c);
                                        }
                                    }
                                });
                                item5.setOnAction(new EventHandler<ActionEvent>() {
                        
                                    @Override
                                    public void handle(ActionEvent event) {
                                        TextInputDialog dialog = new TextInputDialog("Nouvelle arrondissement");
                                        dialog.setTitle("Changement arrondissement");
                                        dialog.setHeaderText("Indiquez le nouvel arrondissement");
                                        Optional<String> result = dialog.showAndWait();
                                        if (result.isPresent()){
                                            System.out.println("Arrondissement: " + result.get());
                                            double newArc = Double.parseDouble(result.get());
                                            Rectangle modif = (Rectangle) (mod);
                                            //modif.setRounded(newArc);
                                            Command c = new ArcCommand(modif,newArc);
                                            c.Execute();
                                            queue.insertUndoQueue(c);
                                        }
                                    }
                                });
                                contextMenu.getItems().addAll(item1,item2,item5);
 
                                contextMenu.show(scene.getWindow(), event.getSceneX()+JavaFXBuilder.toolSize, event.getSceneY()+JavaFXBuilder.menuSize);
                               
                            }
                            if(mod instanceof RegularPolygone){
                                ContextMenu contextMenu = new ContextMenu();
                                MenuItem item1 = new MenuItem("Changer le nombre d'arrete");
                                MenuItem item2 = new MenuItem("Changer le rayon");
                                item1.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        TextInputDialog dialog = new TextInputDialog("Nouveau nombre d'arrete");
                                        dialog.setTitle("Changement nombre d'arrete");
                                        dialog.setHeaderText("Indiquez le nouveau nombre d'arrete");
                                        Optional<String> result = dialog.showAndWait();
                                        if (result.isPresent()){
                                            System.out.println("Arrete: " + result.get());
                                            double newSide = Double.parseDouble(result.get());
                                            RegularPolygone modif = (RegularPolygone) (mod);
                                            System.out.println(mod);
                                            System.out.println(modif);
                                            //modif.changeNbSide(newSide);
                                            Command c = new NbSideCommand(modif,newSide);
                                            c.Execute();
                                            queue.insertUndoQueue(c);
                                        }
                                    }
                                });
                                item2.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        TextInputDialog dialog = new TextInputDialog("Nouveau rayon");
                                        dialog.setTitle("Changement rayon");
                                        dialog.setHeaderText("Indiquez le nouveau rayon");
                                        Optional<String> result = dialog.showAndWait();
                                        if (result.isPresent()){
                                            System.out.println("Rayon: " + result.get());
                                            double newRadius = Double.parseDouble(result.get());
                                            RegularPolygone modif = (RegularPolygone) (mod);
                                            System.out.println(mod);
                                            System.out.println(modif);
                                            //modif.changeRadius(newRadius);
                                            Command c = new RadiusCommand(modif,newRadius);
                                            c.Execute();
                                            queue.insertUndoQueue(c);
                                        }
                                    }
                                });
                                contextMenu.getItems().addAll(item1,item2);
 
                                contextMenu.show(scene.getWindow(), event.getSceneX()+JavaFXBuilder.toolSize, event.getSceneY()+JavaFXBuilder.menuSize);
                            }
                            if(mod instanceof Group){

                            }
                        }
                    System.out.println("right-click\n");
                    
                } else {
                    if (event.getSceneX() < JavaFXBuilder.toolSize) {
                        System.out.println("click");
                        int shapenum = (int) ((event.getSceneY() - JavaFXBuilder.menuSize) / JavaFXBuilder.toolSize);
                        
                            if (shapenum > toolbar.getShape().size() || shapenum <= 0)
                                return;

                            fromBoard = false;
                            select = toolbar.getShape().get(shapenum-1);


                    } else {
                        System.out.println("fromBoard to true.");
                        fromBoard = true;
                        mouseClickX = event.getSceneX();
                        mouseClickY = event.getSceneY();
                    }
                }
            }
        };
        scene.addEventFilter(MouseEvent.MOUSE_PRESSED, pressEvent);

        EventHandler<MouseEvent> releaseEvent = new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                System.out.println("dragDone");
                if (event.getSceneY() < JavaFXBuilder.menuSize || !event.getButton().equals(MouseButton.PRIMARY))
                    return;
                if (select != null) {
                    if (event.getSceneX() <= JavaFXBuilder.toolSize) {
                        int index = (int) (event.getSceneY()/(JavaFXBuilder.toolSize - 1));
                        System.out.println("index = " + index);
                        if(index > 0){
                            try {
                                toolbar.add(select.clone());
                            } catch (CloneNotSupportedException e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                            }
                            select.unselect();
                            select = null;
                        }
                        else if(index == 0){
                            System.out.println("INDEX = 0");
                            if(fromBoard){
                                Command c ;
                                if(!whiteboard.contain(select) && select instanceof Group) {
                                    for (Shape shape : ((Group) select).getListAddress()) {
                                        whiteboard.remove(shape);
                                       c  = new RemoveCommand(whiteboard, shape);
                                       c.Execute();
                                       queue.insertUndoQueue(c);
                                    }
                                } else{
                                    c = new RemoveCommand(whiteboard, select);
                                    c.Execute();
                                    queue.insertUndoQueue(c);
                                }
                                System.out.println("remove from whiteboard");
                            }else{
                                Command c = new RemoveCommand(toolbar, select);
                                c.Execute();
                                queue.insertUndoQueue(c);
                                System.out.println("remove from toolbar");

                            }
                            select = null;
                        }
                    }
                    if (event.getSceneX() > JavaFXBuilder.toolSize) {
                        if (fromBoard) {
                            double transX = event.getSceneX() - select.getRotationCenter().getX();
                            double transY = event.getSceneY() - select.getRotationCenter().getY();
                            Command c = new TranslateCommand(select, transX, transY);
                            queue.insertUndoQueue(c);
                            c.Execute();
                            System.out.println("Move : " + select);
                            select.unselect();
                            select = null;
                        } else {
                            try {
                                Shape clone = select.clone();
                                clone.setPosition(new Point2D(event.getSceneX(), event.getSceneY()));
                                Command c = new AddCommand(whiteboard, clone);
                                queue.insertUndoQueue(c);
                                c.Execute();
                                select = null;
                            } catch (CloneNotSupportedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }
                    System.out.println("fromBoard to false.");
                    fromBoard = false;
                }
                if (event.isControlDown()) {
                    double releaseX = event.getSceneX();
                    double releaseY = event.getSceneY();
                    double minX = (mouseClickX <= releaseX) ? mouseClickX : releaseX;
                    double maxX = (mouseClickX > releaseX) ? mouseClickX : releaseX;
                    double minY = (mouseClickY <= releaseY) ? mouseClickY : releaseY;
                    double maxY = (mouseClickY > releaseY) ? mouseClickY : releaseY;

                    if (select != null)
                        select.unselect();
                        select = (whiteboard.getShapeInSelectArea(minX, minY, maxX, maxY));
                    if(select != null) select.select();
                        System.out.println((select != null) ? select : "null");
                    System.out.println("fromBoard to true.");
                    fromBoard = true;

                    

                }
            }
        };
        scene.addEventFilter(MouseEvent.MOUSE_RELEASED, releaseEvent);

        EventHandler <MouseEvent> clickhandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            if(event.getSceneY() < JavaFXBuilder.menuSize) return;
            if (event.getSceneX() > JavaFXBuilder.toolSize && event.getButton().equals(MouseButton.PRIMARY)) {
                if(event.getClickCount() < 2) return;
                if(select != null) select.unselect();
                select = whiteboard.getShapeByPos(event.getSceneX(), event.getSceneY());
                if (select != null)
                    select.unselect();

                System.out.println((select != null)? select: "null");

            }
            
        }
        };

        scene.addEventHandler(MouseEvent.MOUSE_CLICKED, clickhandler);

    }
    

    
    
    
    
    
    
    
    
    
    
    public void saveWhiteboard(String path){
      
        Command c = new SaveCommand(whiteboard, path);
        c.Execute();
     
    }
    public void loadWhiteboard(String path){
      
        Command c = new LoadCommand(whiteboard, path);
        c.Execute();
     
    }


   public void undo() {
        queue.undo();
    }

    public void redo() {
        queue.redo();
    }

    



}