package ubx.master.gl.board;


import ubx.master.gl.shape.Shape;

public interface Savable {
    public void save(String path);
    
    public void load(String path);
    public void remove(Shape s);
    
    public void add(Shape s);

}