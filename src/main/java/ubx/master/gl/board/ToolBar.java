package ubx.master.gl.board;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ubx.master.gl.json.JsonManager;
import ubx.master.gl.observer.ObserverInterface;
import ubx.master.gl.shape.Color;
import ubx.master.gl.shape.Point2D;
import ubx.master.gl.shape.Rectangle;
import ubx.master.gl.shape.RegularPolygone;
import ubx.master.gl.shape.Shape;

public class ToolBar implements Savable {
    private final int maxSize = 6;
    private final String savePath = "./save/toolbar.aaa";
    private List<Shape> tools;
    private ObserverInterface observer;

    public ToolBar() {
        if(new File(savePath).exists())
            load(savePath);
        else{
            tools = new ArrayList<Shape>();
            Shape defaultRect = new Rectangle(new Point2D(37.5, 37.5), new Color(255, 0, 0), 200, 150, 0);
            Shape defaultPoly = new RegularPolygone(new Point2D(37, 37), new Color(0, 0, 210), 5, 50);
            tools.add(defaultRect);
            tools.add(defaultPoly);

        }
        
    }
    public void addObserver(ObserverInterface observer){
        this.observer = observer;
    }

    @Override
    public void add( Shape s) {
        if (tools.contains(s)) {
            System.out.println("ALREADY EXIST <<<<<<<<<<<<<<<<<<<<<<<<<<<");
            return;
        }
        if(tools.size() < maxSize){
            tools.add(s);
            save(savePath);
            observer.update();
        }
    }
    public void add(Shape s, int index) {
        tools.set(index, s);
        save(savePath);
        observer.update();
    }
    @Override
    public void remove(Shape s) {
        tools.remove(s);
        save(savePath);
        observer.update();
    }
    public List<Shape> getShape(){
        return tools;
    }

    @Override
    public void save(String path) {
        // TODO Auto-generated method stub
        StringBuffer json = new StringBuffer();
        json.append("[");
        for (int i = 0; i < tools.size(); i++) {
            json.append(tools.get(i).toJSON());
            if (i < tools.size() - 1)
                json.append(",");
        }
        json.append("]");

        try {
            FileWriter fw = new FileWriter(path);
            fw.write(json.toString());
            fw.flush();
            fw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public void load(String path) {
        JsonManager manager = new JsonManager();
        tools = manager.extractShape(path);

    }
        
}
