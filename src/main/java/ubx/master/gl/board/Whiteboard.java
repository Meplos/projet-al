package ubx.master.gl.board;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ubx.master.gl.json.JsonManager;
import ubx.master.gl.observer.ObserverInterface;
import ubx.master.gl.shape.AbstractShape;
import ubx.master.gl.shape.Group;
import ubx.master.gl.shape.*;

public class Whiteboard implements Savable {

    private static final long serialVersionUID = 1L;
    private List<Shape> shapes;
    private ObserverInterface observer;

    public Whiteboard() {
        shapes = new ArrayList<Shape>();

    }

    /**
     * @param observer the observer to set
     */
    public void setObserver(ObserverInterface observer) {
        this.observer = observer;
    }


    @Override
    public void add(Shape s) {
        shapes.add(s);
        s.setObserver(observer);
        observer.update();
    }

    @Override
    public void remove(Shape s) {
        shapes.remove(s);
        observer.update();
    }

    public List<Shape> getShapes() {

        List<Shape> list = new ArrayList<Shape>();
        for (Shape shape : shapes) {
            try {
                if (shape instanceof AbstractShape)
                    list.add((Shape) ((AbstractShape) shape).clone());
                if (shape instanceof Group)
                    list.add((Shape) ((Group) shape).clone());

            } catch (Exception e) {
                throw new RuntimeException();
            }

        }
        return list;

    }

    public Shape getShapeInSelectArea(double topX, double topY, double botX, double botY) {
        Shape res = null;
        List<Shape> selection = new ArrayList<>();
        for (Shape shape : shapes) {
            if(shape.isInSelectedArea(topX, topY, botX, botY))
            selection.add(shape);
        }
        if(selection.size() == 1) {
            res = selection.get(0);
        } else if(selection.size() > 1 ) {
            res = new Group(selection);
        }
        return res;
    }

    public Shape getShapeByPos( double x, double y) {
        for (Shape shape : shapes) {
            if (shape.isIn(x,y)){
                shape.select();
                return shape;
            }
        }
        return null;
    }

    public boolean containList(List<Shape> list){
        return shapes.containsAll(list);
        
    }

    public boolean contain(Shape s){
        return shapes.contains(s);
    }


    @Override
    public void save(String path) {
        StringBuffer json = new StringBuffer();
        json.append("[");
        for (int i = 0; i < shapes.size(); i++) {
            json.append(shapes.get(i).toJSON());
            if(i < shapes.size()-1)
                json.append(",");
        }
        json.append("]");

        try {
            FileWriter fw = new FileWriter(path);
            fw.write(json.toString());
            fw.flush();
            fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }

    @Override
    public void load(String path) {
        JsonManager manager = new JsonManager();
        shapes = manager.extractShape(path);
        observer.update();

    }
        
}
