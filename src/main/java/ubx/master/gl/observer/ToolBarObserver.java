package ubx.master.gl.observer;

import ubx.master.gl.board.ToolBar;
import ubx.master.gl.builder.Builder;
import ubx.master.gl.builder.JavaFXBuilder;

public class ToolBarObserver implements ObserverInterface {

    private ToolBar toolbar;
    private Builder builder;
    public ToolBarObserver(ToolBar bar,Builder b){
       this.toolbar = bar;
       this.builder = b;
    }
 
    @Override
    public void update() {
        builder.buildToolBar(toolbar);
        ((JavaFXBuilder) builder).getResult();
        
        //s.setScene(((JavaFXBuilder) b).getResult());
        
    }
 }