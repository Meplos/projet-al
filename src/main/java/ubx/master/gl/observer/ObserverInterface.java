package ubx.master.gl.observer;


public interface ObserverInterface{

    public void update();

}
