package ubx.master.gl.observer;

import ubx.master.gl.board.Whiteboard;
import ubx.master.gl.builder.Builder;
import ubx.master.gl.builder.JavaFXBuilder;


public class WhiteboardObserver implements ObserverInterface {

    private Whiteboard board;
    private Builder builder;

    public WhiteboardObserver(Whiteboard b, Builder build){
       this.board = b;
        this.builder = build;
    }
    
    @Override
    public void update() {
        builder.buildWhiteboard(board);
        ((JavaFXBuilder) builder).getResult();
        System.out.println("refresh");
    }
 }