package ubx.master.gl.builder;

import ubx.master.gl.board.ToolBar;
import ubx.master.gl.board.Whiteboard;

public interface Builder {

    public void buildMenuBar();
    public void buildToolBar(ToolBar toolbar);
    public void buildWhiteboard(Whiteboard whiteboard);

}