package ubx.master.gl.builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;

import javafx.scene.layout.VBox;

import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ubx.master.gl.api.JavaFXCanvas;
import ubx.master.gl.api.JavaFXEventManager;
import ubx.master.gl.board.ToolBar;
import ubx.master.gl.board.Whiteboard;

import ubx.master.gl.command.Command;
import ubx.master.gl.command.CommandQueue;
import ubx.master.gl.command.LoadCommand;
import ubx.master.gl.command.SaveCommand;

import ubx.master.gl.shape.Shape;

public class JavaFXBuilder implements Builder {

    public static final double menuSize = 40;
    public static final double toolSize = 75;
    private BorderPane layout;
    private Stage stage;
    Whiteboard w;
    ToolBar tools;
    CommandQueue queue;
    JavaFXEventManager evtManager;

    @Override
    public void buildMenuBar() {
        Button saveButton = new Button("Save");
        Button loadButton = new Button("Load");
        Button undoButton = new Button("Undo");
        Button redoButton = new Button("Redo");

        javafx.scene.control.ToolBar menuBar = new javafx.scene.control.ToolBar(saveButton, loadButton, undoButton,
                redoButton);
        menuBar.setMaxHeight(menuSize);
        menuBar.setMinHeight(menuSize);
        EventHandler<MouseEvent> saveHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                File f = new FileChooser().showSaveDialog(stage);
                if (f != null) {
                    String path = f.getPath();
                    Command c = new SaveCommand(w, path);
                    c.Execute();
                }
            }
        };
        EventHandler<MouseEvent> loadHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                File f = new FileChooser().showOpenDialog(stage);
                if (f != null) {
                    Command c = new LoadCommand(w, f.getPath());
                    c.Execute();
                }
            }
        };

        EventHandler<MouseEvent> undoHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                evtManager.undo();
            }
        };

        EventHandler<MouseEvent> redoHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                evtManager.redo();
            }
        };

        saveButton.setOnMouseClicked(saveHandler);
        loadButton.setOnMouseClicked(loadHandler);
        undoButton.setOnMouseClicked(undoHandler);
        redoButton.setOnMouseClicked(redoHandler);

        menuBar.setOrientation(Orientation.HORIZONTAL);
        layout.setTop(menuBar);

    }

    @Override
    public void buildToolBar(ToolBar toolbar) {
        tools = toolbar;
        VBox vbox = new VBox();
        try {
            Image trash = new Image(new FileInputStream("./src/assets/trash.png"));
            ImageView trashView = new ImageView(trash);
            trashView.setFitHeight(75);
            trashView.setFitWidth(75);
            trashView.setPreserveRatio(false);
            vbox.getChildren().add(trashView);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        vbox.setBackground(
                new Background(new BackgroundFill(javafx.scene.paint.Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        vbox.setMaxWidth(75);
        vbox.setMinWidth(75);

        for (Shape s : toolbar.getShape()) {
            JavaFXCanvas canevas = new JavaFXCanvas(75, 75);
            s.drawThumbnail(canevas);
            vbox.getChildren().add(canevas);
        }
        layout.setLeft(vbox);
    }

    @Override
    public void buildWhiteboard(Whiteboard whiteboard) {
        JavaFXCanvas canvas = new JavaFXCanvas(this.stage.getWidth() - 40, this.stage.getHeight() - 20);
        for (Shape shape : whiteboard.getShapes()) {
            shape.draw(canvas);
        }
        System.out.println(canvas);
        this.layout.setCenter(canvas);
        System.out.println(this.layout.getCenter());
        System.out.println("UPDATE WH");

    }

    public void getResult() {
        stage.show();
        System.out.println("SHOW");

    }

    public JavaFXBuilder(Stage s, Whiteboard whiteboard, ToolBar toolbar) {
        this.w = whiteboard;
        this.tools = toolbar;
        this.layout = new BorderPane();
        this.stage = s;
        Scene scene = new Scene(this.layout);        
        this.stage.setScene(scene);
        this.queue = new CommandQueue();
        this.evtManager = JavaFXEventManager.getInstance(scene, w, toolbar);
        evtManager.initEvent();
    }

}