package ubx.master.gl.shape;

import java.io.Serializable;

import ubx.master.gl.api.*;
import ubx.master.gl.observer.ObserverInterface;

public interface Shape extends Serializable, Cloneable{

    /*@brief rotate shape to an angle°
      @params angle rotating angulus
    */
    public void rotate(double angle);
    /*
     * @brief translate shape
     * @params transX translation distance on x axis
     * @params transY translation distance on y axis
     * 
     */

    public void translate(double transX, double transY);

    
    public void setPosition(Point2D clone);
    /*
     * @brief draw shape
     * 
     * @params frame, where is draw the shape 
     */

    public void draw(FrameApi frame);
    
    public Shape clone() throws CloneNotSupportedException;

    /*
     * @brief draw a thumbnail of the shape
     * @params frame, where is draw the shape
     */

    public void drawThumbnail(FrameApi frame);
    
    /*
     * @brief return if point x,y is inside the shape /* @brief return if point x,y
     * @params x, x coordinate
     * @params y, y coordinate
     * @return true if (x,y) is inside the shape  
     */
    public boolean isIn(double x, double y);
    
    /*
     * @brief return if shape is inside the area
     * 
     * @params minX, left corner of the area
     * 
     * @params minY, top corner of the area
     * 
     * @params maxX, right corner of the area
     * 
     * @params maxY, bot corner of the area
     * 
     * @return true if the center point of the shape is inside the area describe by parameters
     */
    public boolean isInSelectedArea(double minX, double minY, double maxX, double maxY );
    /*
        @brief select the shape
    */ 
    public void select();
    /*
     * @brief unselect the shape
     */

    public void unselect();
    /*
        @return json string description of the 
    */ 
    public String toJSON();
    public void setColor(Color color);
    public void setObserver(ObserverInterface obs);
    public Point2D getRotationCenter();


}
