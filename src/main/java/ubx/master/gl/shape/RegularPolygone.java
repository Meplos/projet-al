package ubx.master.gl.shape;

import java.util.ArrayList;
import java.util.List;

import ubx.master.gl.api.JavaFXDraw;
import ubx.master.gl.observer.ObserverInterface;

public class RegularPolygone extends AbstractShape {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<Point2D> points;
    private double nbSide;
    private double radius;
    private double angle;

    public RegularPolygone(Point2D rotationCenter, Color color, double nbSide, double radius) {
        super(rotationCenter, rotationCenter, color);
        double angle = 2 * Math.PI / nbSide;
        points = new ArrayList<Point2D>();
        for (int i = 0; i < nbSide; i++) {
            double x = rotationCenter.getX() + radius * Math.sin(i * angle);
            double y = rotationCenter.getY() + radius * Math.cos(i * angle);
            points.add(new Point2D(x, y));
        }
        super.setPosition(points.get(0));
        this.nbSide = nbSide;
        this.radius = radius;
        this.angle = 0;
        try {
            super.setDrawApi(new JavaFXDraw(cloneList(), (Point2D) getRotationCenter().clone(), 0, angle,
            (Color) this.getColor().clone(), false));
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        rotate(angle);
    }

    public RegularPolygone(Point2D rotationCenter, Color color, double nbSide, double radius, List<Point2D> pts, double angle) {
        super(rotationCenter, rotationCenter, color);
        points = pts;
        for (int i = 0; i < nbSide; i++) {
            double x = pts.get(i).getX();
            double y = pts.get(i).getY();
            points.add(new Point2D(x, y));
        }
        super.setPosition(points.get(0));
        this.nbSide = nbSide;
        this.radius = radius;
        this.angle = angle;
        try {
            super.setDrawApi(new JavaFXDraw(cloneList(), (Point2D) getRotationCenter().clone(), 0, 0,
                    (Color) this.getColor().clone(), false));
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        rotate(angle);
    }

    public void changeNbSide(double newNbSide) {
        this.nbSide = newNbSide;
        double angle = 2 * Math.PI / nbSide;
        points = new ArrayList<Point2D>();
        for (int i = 0; i < nbSide; i++) {
            double x = getRotationCenter().getX() + radius * Math.sin(i * angle);
            double y = getRotationCenter().getY() + radius * Math.cos(i * angle);
            points.add(new Point2D(x, y));
        }
        super.setPosition(points.get(0));
        try {
            super.setDrawApi(new JavaFXDraw(cloneList(), (Point2D) getRotationCenter().clone(), 0, angle,
                    (Color) this.getColor().clone(), false));
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        notifyObs();
    }

    public void changeRadius(double newRadius) {
        this.radius = newRadius;
        double angle = 2 * Math.PI / nbSide;
        points = new ArrayList<Point2D>();
        for (int i = 0; i < nbSide; i++) {
            double x = getRotationCenter().getX() + radius * Math.sin(i * angle);
            double y = getRotationCenter().getY() + radius * Math.cos(i * angle);
            points.add(new Point2D(x, y));
        }
        super.setPosition(points.get(0));
        try {
            super.setDrawApi(new JavaFXDraw(cloneList(), (Point2D) getRotationCenter().clone(), 0, angle,
                    (Color) this.getColor().clone(), false));
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        rotate(angle);
        notifyObs();
    }

    @Override
    public void setPosition(Point2D position) {
        super.setRotationCenter(position);

        double angle = 2 * Math.PI / nbSide;
        points.clear();
        for (int i = 0; i < nbSide; i++) {
            double x = getRotationCenter().getX() + radius * Math.sin(i * angle);
            double y = getRotationCenter().getY() + radius * Math.cos(i * angle);
            points.add(new Point2D(x, y));
        }
        super.setPosition(points.get(0));
        rotate(angle);

        try {
            getDrawApi().setPoint2D(cloneList());
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        rotate(angle);
    }

    @Override
    public void translate(double transX, double transY) {
        super.translate(transX, transY);
        List<Point2D> pts = new ArrayList<>();
        for (int i = 0; i < this.points.size(); i++) {
            Point2D newPoint = new Point2D(this.points.get(i).getX() + transX, this.points.get(i).getY() + transY);
            points.set(i, newPoint);
        }
        this.points = pts;
        try {
            getDrawApi().setPoint2D(cloneList());
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        notifyObs();
    }

    @Override
    public void rotate(double angle) {
        super.rotate(angle);

        double theta = Math.toRadians(angle % 360);

        for (int i = 0; i < nbSide; i++) {
            // Centrer la forme pour que le centre soit l'origine du repère
            double x = points.get(i).getX() - getRotationCenter().getX();
            double y = points.get(i).getY() - getRotationCenter().getY();

            // rotate
            double rotateX = x * Math.cos(theta) - y * Math.sin(theta);
            double rotateY = x * Math.sin(theta) + y * Math.cos(theta);

            // replace shape
            double newX = rotateX + getRotationCenter().getX();
            double newY = rotateY + getRotationCenter().getY();
            Point2D newPoint = new Point2D(newX, newY);
            points.set(i, newPoint);
        }
        try {
            getDrawApi().setPoint2D(cloneList());
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        notifyObs();
    
    }

    public List<Point2D> cloneList() throws CloneNotSupportedException {
        List<Point2D> cloneObject = new ArrayList<>();
        for (Point2D p : points) {
            Point2D cpy = null;
            cpy = (Point2D) p.clone();
            cloneObject.add(cpy);
        }
        return cloneObject;
    }

    public void setList(List<Point2D> points){
        this.points = points;
    }

    @Override
    public RegularPolygone clone() throws CloneNotSupportedException {
        RegularPolygone cpy = new RegularPolygone(new Point2D(this.getRotationCenter().getX(),this.getRotationCenter().getY()),
                                                    this.getColor(),
                                                    this.getNbSide(),
                                                    this.getRadius());
            for (ObserverInterface obs : super.getObservers()) {
                cpy.setObserver(obs);
            }
        return cpy;
    }

    public double getNbSide() {
        return nbSide;
    }

    public void setNbSide(double nbSide) {
        this.nbSide = nbSide;
        notifyObs();
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public List<Point2D> getPoints(){
        return points;
    }

    public double getAngle() {
        return angle;
    }
    @Override
    public String toJSON(){
        StringBuffer json = new StringBuffer();
        json.append("{\"type\": \"regularPolygone\",");
        json.append(super.toJSON());
        json.append("\"nbSide\":"+getNbSide()+",");
        json.append("\"radius\":"+getRadius()+",");
        json.append("\"angle\":" + getAngle() + ",");
        json.append("\"points\":[");
        for (int i = 0; i < points.size(); i++) {
            json.append(points.get(i).toJSON());
            if(i < points.size()-1)
                json.append(",");
        }
        json.append("]");
        json.append("}");
        return json.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(radius);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(nbSide);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        RegularPolygone other = (RegularPolygone) obj;
        if (Double.doubleToLongBits(radius) != Double.doubleToLongBits(other.radius))
            return false;
        if (Double.doubleToLongBits(nbSide) != Double.doubleToLongBits(other.nbSide))
            return false;
        return super.equals(obj);
    }

    @Override
    public boolean isIn(double x, double y) {
        return (x >= getRotationCenter().getX() - getRadius()
            && x <= getRotationCenter().getX() + getRadius()
            && y >= getRotationCenter().getY() - getRadius() 
            && y <=getRotationCenter().getY() + getRadius());
    }

    @Override
    public void setColor(Color color) {
        setColor(color);
    }

}