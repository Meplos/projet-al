package ubx.master.gl.shape;

import java.util.List;
import java.util.ArrayList;
import ubx.master.gl.api.*;
import ubx.master.gl.observer.*;

public abstract class AbstractShape implements Shape {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private DrawApi drawapi;
    private Point2D position;
    private Point2D rotationCenter;
    private Color color;
    private List<ObserverInterface> obs;
    private boolean isSelect;

    public AbstractShape(Point2D position, Point2D rotationCenter, Color color) {
        this.position = position;
        this.rotationCenter = rotationCenter;
        this.color = color;
        this.obs = new ArrayList<>();
        isSelect = false;
    }

    public AbstractShape(Point2D position, Point2D rotationCenter, Color color, DrawApi drawapi) {
        this.position = position;
        this.rotationCenter = rotationCenter;
        this.color = color;
        this.drawapi = drawapi;
        this.obs = new ArrayList<>();
        isSelect = false;

    }

	public void setObserver(ObserverInterface obs) {
        if(!getObservers().contains(obs))
            this.obs.add(obs);
    }

    public void notifyObs() {
        for (ObserverInterface observerInterface : obs) {
            observerInterface.update();
        }
    }

    public List<ObserverInterface> getObservers() {
        return this.obs;
    }

    public void setDrawApi(DrawApi drawapi) {
        this.drawapi = drawapi;
    }

    @Override
    public void rotate(double angle) {
        double theta = Math.toRadians(angle % 360);
        // Centrer la forme pour que le centre soit l'origine du repère
        double x = this.position.getX() - this.rotationCenter.getX();
        double y = this.position.getY() - this.rotationCenter.getY();

        // rotate
        double rotateX = x * Math.cos(theta) - y * Math.sin(theta);
        double rotateY = x * Math.sin(theta) + y * Math.cos(theta);

        // replace shape
        double newX = rotateX + this.rotationCenter.getX();
        double newY = rotateY + this.rotationCenter.getY();
        System.out.println("X: " + newX + " Y: " + newY);
        this.position = new Point2D(newX, newY);
        notifyObs();

    }

    @Override
    public void translate(double transX, double transY) {
        Point2D finalPos = new Point2D(this.position.getX() + transX, this.position.getY() + transY);
        Point2D RotaPos = new Point2D(this.rotationCenter.getX() + transX, this.rotationCenter.getY() + transY);
        this.position = finalPos;
        this.rotationCenter = RotaPos;
    }

    @Override
    public boolean isInSelectedArea(double minX, double minY, double maxX, double maxY) {
        return (rotationCenter.getX() >= minX 
                && rotationCenter.getX() <= maxX 
                && rotationCenter.getY() >= minY
                && rotationCenter.getY() <= maxY);
    }

    @Override
    public void select() {
        this.isSelect = true;
        System.out.println("Abstract Shape select : " + isSelect);
        drawapi.setSelection(isSelect);
        notifyObs();

    }

    @Override
    public void unselect() {
        this.isSelect = false;
        drawapi.setSelection(isSelect);
        notifyObs();
    }

    @Override
    public void draw(FrameApi frame) {
        drawapi.draw(frame);
        return;
    }

    @Override
    public void drawThumbnail(FrameApi frame) {
        drawapi.drawThumbnail(frame);
    }


    public DrawApi getDrawApi() {
        return this.drawapi;
    }

    public Point2D getPosition() {
        return this.position;
    }

    public void setPosition(Point2D position) {
        this.position = position;
    }

    public Point2D getRotationCenter() {
        try {
            return (Point2D) rotationCenter.clone();
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public void setRotationCenter(Point2D rotationCenter) {
        this.rotationCenter = rotationCenter;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color newColor) {
        this.color = newColor;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((position == null) ? 0 : position.hashCode());
        result = prime * result + ((rotationCenter == null) ? 0 : rotationCenter.hashCode());
        return result;
    }

    public List<Point2D> getPoints(){
        List<Point2D> points = new ArrayList<Point2D>();
        points.add(position);
        return points;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractShape other = (AbstractShape) obj;
        if (color == null) {
            if (other.color != null)
                return false;
        } else if (!color.equals(other.color))
            return false;
        if (position == null) {
            if (other.position != null)
                return false;
        } else if (!position.equals(other.position))
            return false;
        if (rotationCenter == null) {
            if (other.rotationCenter != null)
                return false;
        } else if (!rotationCenter.equals(other.rotationCenter))
            return false;
        return true;
    }

    @Override
    public AbstractShape clone() throws CloneNotSupportedException {
        Shape c = (Shape) super.clone();
        ((AbstractShape) c).setPosition(new Point2D(this.position.getX(),this.position.getY()));
        ((AbstractShape) c).setRotationCenter(new Point2D(this.rotationCenter.getY(),this.rotationCenter.getY()));
        ((AbstractShape) c).setColor((Color) this.color.clone());
        return (AbstractShape) c;
    }

    @Override
    public String toJSON() {
        StringBuffer json = new StringBuffer();
        json.append("\"position\":"+position.toJSON()+",");
        json.append("\"rotationCenter\":"+rotationCenter.toJSON()+",");
        json.append("\"color\":"+color.toJSON()+",");

        return json.toString();

    }

}