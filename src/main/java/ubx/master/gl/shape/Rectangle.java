package ubx.master.gl.shape;

import java.util.ArrayList;
import java.util.List;

import ubx.master.gl.api.JavaFXDraw;
import ubx.master.gl.observer.ObserverInterface;

public class Rectangle extends AbstractShape {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<Point2D> points;
    private double width;
    private double height;
    private double rounded;
    private double angle;

    public Rectangle(Point2D position, Point2D rotationCenter, Color color, double width, double height,
            double rounded) {
        super(position, rotationCenter, color);
        this.width = width;
        this.height = height;
        this.rounded = 0;
        System.out.println("rounded" + rounded);
        points = new ArrayList<Point2D>();
        points.add(position);
        points.add(new Point2D(getPosition().getX() + width, getPosition().getY()));
        points.add(new Point2D(getPosition().getX() + width, getPosition().getY() + height));
        points.add(new Point2D(getPosition().getX(), getPosition().getY() + height));
        angle = 0;
        try {
            super.setDrawApi(new JavaFXDraw(cloneList(), (Point2D) getRotationCenter().clone(), rounded, angle,
            (Color) this.getColor().clone(), false));
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        rotate(angle);
    }

    public Rectangle(Point2D rotationCenter, Color color, double width, double height, double rounded) {
        super(new Point2D(rotationCenter.getX() - width / 2, rotationCenter.getY() - height / 2), rotationCenter,
                color);
        this.width = width;
        this.height = height;
        this.rounded = rounded;
        points = new ArrayList<Point2D>();
        points.add(new Point2D(rotationCenter.getX() - width / 2, rotationCenter.getY() - height / 2));
        points.add(new Point2D(rotationCenter.getX() + width / 2, rotationCenter.getY() - height / 2));
        points.add(new Point2D(rotationCenter.getX() + width / 2, rotationCenter.getY() + height / 2));
        points.add(new Point2D(rotationCenter.getX() - width / 2, rotationCenter.getY() + height / 2));
        angle = 0;
        try {
            super.setDrawApi(new JavaFXDraw(cloneList(), (Point2D) getRotationCenter().clone(), rounded, angle,
                    (Color) this.getColor().clone(), false));
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        rotate(angle);

    }

    public Rectangle(Point2D position, Point2D rotationCenter, Color color, double width, double height, double rounded,
            List<Point2D> pts, double angle) {
        super(position, rotationCenter, color);
        this.width = width;
        this.height = height;
        this.rounded = rounded;
        this.points = pts;
        this.angle = angle;
        try {
            super.setDrawApi(new JavaFXDraw(cloneList(), (Point2D) getRotationCenter().clone(), rounded, angle,
                    (Color) this.getColor().clone(), false));
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        rotate(angle);
        

    }

    @Override
    public void rotate(double angle) {
        double theta = Math.toRadians(angle % 360);
        List<Point2D> newPoint = new ArrayList<>();
        for (Point2D point2d : points) {
            System.out.println("X: " + point2d.getX() + " Y: " + point2d.getY());

            // Centrer la forme pour que le centre soit l'origine du repère
            double x = point2d.getX() - getRotationCenter().getX();
            double y = point2d.getY() - getRotationCenter().getY();

            // rotate
            double rotateX = x * Math.cos(theta) - y * Math.sin(theta);
            double rotateY = x * Math.sin(theta) + y * Math.cos(theta);

            // replace shape
            double newX = rotateX + getRotationCenter().getX();
            double newY = rotateY + getRotationCenter().getY();
            System.out.println("newX: " + newX + " newY: " + newY);
            newPoint.add(new Point2D(newX, newY));
        }

        this.points = newPoint;
        this.angle = angle;

        try {
            getDrawApi().setPoint2D(cloneList());
            getDrawApi().setAngle(this.angle);
            notifyObs();
            System.out.println("Rotate rect");
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void changeWidth(double newWidth){
        this.width = newWidth;
        points = new ArrayList<Point2D>();

        //Important pour la hitbox
        setPosition(new Point2D(getRotationCenter().getX() - width / 2, getRotationCenter().getY() - height / 2));

        points.add(new Point2D(getRotationCenter().getX() - width / 2, getRotationCenter().getY() - height / 2));
        points.add(new Point2D(getRotationCenter().getX() + width / 2, getRotationCenter().getY() - height / 2));
        points.add(new Point2D(getRotationCenter().getX() + width / 2, getRotationCenter().getY() + height / 2));
        points.add(new Point2D(getRotationCenter().getX() - width / 2, getRotationCenter().getY() + height / 2));
        rotate(angle);
    }

    public void changeHeight(double newHeight){
        this.height = newHeight;
        points = new ArrayList<Point2D>();
        setPosition(new Point2D(getRotationCenter().getX() - width / 2, getRotationCenter().getY() - height / 2));
        points.add(new Point2D(getRotationCenter().getX() - width / 2, getRotationCenter().getY() - height / 2));
        points.add(new Point2D(getRotationCenter().getX() + width / 2, getRotationCenter().getY() - height / 2));
        points.add(new Point2D(getRotationCenter().getX() + width / 2, getRotationCenter().getY() + height / 2));
        points.add(new Point2D(getRotationCenter().getX() - width / 2, getRotationCenter().getY() + height / 2));
        rotate(angle);
    }

    @Override
    public void translate(double transX, double transY) {
        super.translate(transX, transY);
        System.out.println("RECTANGLE  POINT BEFORE: ");
        for (Point2D point2d : this.points) {

            System.out.println(point2d.getX() + " " + point2d.getY());
        }
        List<Point2D> newPts = new ArrayList<>();
        for (Point2D point2d : points) {
            double newX = point2d.getX() + transX;
            double newY = point2d.getY() + transY;
            newPts.add(new Point2D(newX, newY));
        }
        this.points = newPts;
        try {
            List<Point2D> cpy = cloneList();
            System.out.println("RECTANGLE : ");
            for (Point2D point2d : cpy) {
                
                System.out.println(point2d.getX() + " " + point2d.getY());
            }
            getDrawApi().setPoint2D(cpy);
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        notifyObs();

    }
 
    @Override
    public void setPosition(Point2D position) {
        super.setRotationCenter(position);
        super.setPosition(new Point2D(getRotationCenter().getX() - width / 2, getRotationCenter().getY() - height / 2));
        System.out.println(position.getX() - width / 2);
        System.out.println(position.getY() - height / 2);
        System.out.println(width);
        System.out.println(height);
        points.clear();
        System.out.println(points.size());
        points.add(getPosition());
        points.add(new Point2D(getPosition().getX() + width, getPosition().getY()));
        points.add(new Point2D(getPosition().getX() + width, getPosition().getY() + height)); 
        points.add(new Point2D(getPosition().getX(), getPosition().getY() + height)); 
        System.out.println(points.size());
        rotate(angle);

    }

    @Override
    public Rectangle clone() throws CloneNotSupportedException {
        Rectangle cpy = new Rectangle(new Point2D(this.getPosition().getX(),this.getPosition().getY()),
                                    new Point2D(this.getRotationCenter().getX(),this.getRotationCenter().getY()),
                                    this.getColor(),
                                    this.getWidth(),
                                    this.getHeight(),
                                    this.getRounded()
                                    );
        for (ObserverInterface obs : this.getObservers()) {
            this.setObserver(obs);
        }
        return cpy;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
        notifyObs();
    }

    public double getRounded() {
        return rounded;
    }

    public void setRounded(double rounded) {
        this.rounded = rounded;
        getDrawApi().setArc(rounded);
        notifyObs();
    }

    public double getAngle(){
        return angle;
    }

    //see https://math.stackexchange.com/questions/190111/how-to-check-if-a-point-is-inside-a-rectangle
    
    private double sign(Point2D p1, Point2D p2, Point2D p3){
         return (p1.getX() - p3.getX()) * (p2.getY() - p3.getY()) - (p2.getX() - p3.getX()) * (p1.getY() - p3.getY());
    }
    private boolean inTriangle(Point2D pt, Point2D a1, Point2D a2, Point2D a3){
        boolean b1, b2, b3;
        b1 = sign(pt, a1, a2) < 0;
        b2 = sign(pt, a2, a3) < 0;
        b3 = sign(pt, a3, a1) < 0;

        return ((b1 == b2) && (b2 == b3)); 
    }
    @Override
    public boolean isIn(double x,double y){
        boolean inFirstTriangle, inSecondTriangle;
        Point2D pt = new Point2D(x,y);
        inFirstTriangle = inTriangle(pt,points.get(0),points.get(1),points.get(2));
        inSecondTriangle = inTriangle(pt,points.get(0),points.get(3),points.get(2));
        return inFirstTriangle || inSecondTriangle; 
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////
    public List<Point2D> cloneList() throws CloneNotSupportedException {
        List<Point2D> cloneObject = new ArrayList<>();
        for (Point2D p : points) {
            Point2D cpy = null;
            cpy = (Point2D) p.clone();
            cloneObject.add(cpy);
        }
        return cloneObject;
    }


@Override
public String toJSON() {
    StringBuffer json = new StringBuffer();
    json.append("{\"type\": \"rectangle\",");
    json.append(super.toJSON());
    json.append("\"width\":"+getWidth()+",");
    json.append("\"height\":"+getHeight()+",");
    json.append("\"rounded\":"+getRounded()+",");
    json.append("\"angle\" : " + angle +",");
    json.append("\"points\":[");
    for (int i = 0; i < points.size(); i++) {
        json.append(points.get(i).toJSON());
        if(i < points.size()-1)
            json.append(",");
    }
    json.append("]");
    json.append("}");

    return json.toString();
}
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(height);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(rounded);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(width);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public List<Point2D> getPoints(){
        return points;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Rectangle other = (Rectangle) obj;
        if (Double.doubleToLongBits(height) != Double.doubleToLongBits(other.height))
            return false;
        if (Double.doubleToLongBits(rounded) != Double.doubleToLongBits(other.rounded))
            return false;
        if (Double.doubleToLongBits(width) != Double.doubleToLongBits(other.width))
            return false;
        return super.equals(obj);
    }

    @Override
    public void setColor(Color color) {
        setColor(color);
    }

    
}