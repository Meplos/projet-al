package ubx.master.gl.shape;

import java.io.Serializable;

public class Color implements Cloneable,Serializable{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int r;
    private int v;
    private int b;

    public Color(int r, int v, int b) {
        this.r = r % 256;
        this.v = v % 256;
        this.b = b % 256;
    }

    public int getR() {
        return r;
    }

    public int getV() {
        return v;
    }

    public int getB() {
        return b;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + b;
        result = prime * result + r;
        result = prime * result + v;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Color other = (Color) obj;
        if (b != other.b)
            return false;
        if (r != other.r)
            return false;
        if (v != other.v)
            return false;
        return true;
    }

    public String toJSON() {
        return "{\"r\": "+ getR()+", \"v\":"+ getV() + ", \"b\": "+ getB() +" }" ;
    }
    
}