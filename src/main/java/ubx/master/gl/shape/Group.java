package ubx.master.gl.shape;

import java.util.ArrayList;
import java.util.List;

import ubx.master.gl.api.FrameApi;
import ubx.master.gl.observer.ObserverInterface;

public class Group implements Shape {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<Shape> shapes;

    @Override
    public Group clone() throws CloneNotSupportedException {
        
        Group cpy = new Group();
        for(Shape s : shapes){
            cpy.addShape(s.clone());
        }
        return cpy;
    }

    public void setList(List<Shape> list) {
        this.shapes = list;
    }

    public void addShape(Shape s) {
        shapes.add(s);
    }

    public void removeShape(Shape s) {
        shapes.remove(s);
    }
    public List<Shape> getListAddress(){
        return shapes;
    }

    public List<Shape> getList() throws CloneNotSupportedException {
        List<Shape> cloneObject = new ArrayList<>();
        for (Shape shape : shapes) {
            Shape cpy = null;
                cpy = (Shape)  shape.clone();

            cloneObject.add(cpy);

        }
        return cloneObject;
    }

    public Shape getShapeByPos(double x, double y) {
        for (Shape shape : shapes) {
            if (shape.isIn(x, y))
                return shape;
        }
        return null;
    }

    @Override
    public void rotate(double angle) {
        for (Shape shape : shapes) {
            shape.rotate(angle);
        }
    }

    @Override
    public void setPosition(Point2D clone) {
        Point2D center = getRotationCenter();
        double deltaX = Math.abs(clone.getX() - center.getX());
        double deltaY = Math.abs(clone.getY() - center.getY());
        translate(deltaX, deltaY);
    }


    public void translate(double transX, double transY ){
        for (Shape shape : shapes) {
            shape.translate(transX, transY);
        }
    }

    public Group(List<Shape> shapes) {
        this.shapes = shapes;
    }

    public Group() {
        this.shapes = new ArrayList<>();
    }

    @Override
    public void draw(FrameApi frame) {
        for (Shape shape : shapes) {
            shape.draw(frame);
        }
        return;
    }

    @Override
    public void drawThumbnail(FrameApi frame) {
        for (Shape shape : shapes) {
            shape.drawThumbnail(frame);
        }
        return;
    }

    @Override
    public boolean isIn(double x, double y) {
        for (Shape shape : shapes) {
            if (shape.isIn(x, y))
                return true;
        }
        return false;
    }

    /*@return rotation center of the group*/
    @Override
    public Point2D getRotationCenter() {
        double xMin = 900, xMax = 0, yMin = 600, yMax = 0 ;
        for(Shape shape : shapes) {
            if(shape.getRotationCenter().getX() <= xMin){
                xMin = shape.getRotationCenter().getX();
            }
            if(shape.getRotationCenter().getX() >= xMax){
                xMax = shape.getRotationCenter().getX();
            }
            if(shape.getRotationCenter().getY() <= yMin){
                yMin = shape.getRotationCenter().getX();
            }
            if(shape.getRotationCenter().getY() <= xMax){
                yMax = shape.getRotationCenter().getX();
            }
        }
        return new Point2D((xMin + xMax)/2,(yMin + yMax)/2);
    }

    @Override
    public boolean isInSelectedArea(double minX, double minY, double maxX, double maxY) {
        for (Shape shape : shapes) {
            if(shape.isInSelectedArea(minY,minY, maxX,maxY)) return true; 
        }
        return false;
    }


    @Override
    public void select() {
        for (Shape shape : shapes) {
            shape.select();
        }
    }

    @Override
    public void unselect() {
        for (Shape shape : shapes) {
            shape.unselect();
        }
    }

    @Override
    public String toJSON() {
        StringBuffer json = new StringBuffer();
        json.append("{\"type\" : \"group\", \"shapes\" : [");
        for (int i = 0; i < shapes.size(); i++) {
            json.append(shapes.get(i).toJSON());
            if (i != shapes.size() - 1) {
                json.append(",");
            }
        }
        json.append("]");
        json.append("}");
        return json.toString();

    }

    @Override
    public void setColor(Color newColor) {
        for (Shape shape : shapes) {
            shape.setColor(newColor);
        }

    }

    @Override
    public void setObserver(ObserverInterface obs) {
        for (Shape shape : shapes) {
            shape.setObserver(obs);
        }
    }


    
}