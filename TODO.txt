Sujet : https://www.labri.fr/perso/auber/ALM1GL/tds/projet/index.html

Pour le 20 mai (et plus si autorisé) :
TO DO:
-sélectionner un objet depuis une toolbar
-drag and drop la figure
-modifier la taille, position, etc... de nos objets ou groupes d'objets une fois ceux-ci incorporés dans le dessin, en les selectionnant d'un clic gauche, puis ensuite menu déroulant via clic droit, plus clic gauche sur "Edit", pour pouvoir modifier, une boîte de dialogue s'ouvrira où on pourra choisir quels attributs modifier, puis on peut cliquer sur "Apply" pour appliquer les modifs et les continuer, "Ok" pour appliquer et fermer la boîte de dialogue, ou "Cancel" pour tout annuler depuis l'ouverture de la boîte de dialogue
-faire en sorte que l'utilisateur puisse modifier les propriétés du rectangle : largeur, hauteur, rotation, position, centre de rotation, translation, couleur, arrondi des bords
-forte en sorte que l'utilisateur puisse modifier les propriétés du polygone régulier : nombre de côtés, longueur d'un côté, position, rotation, translation, couleur
-Créer des groupes d’objets et sous-groupes d’objets, sous-sous-groupes etc… grouper les objets, soit en restant appuyé sur le clic en déplaçant la souris pour faire apparaître un rectangle de sélection, soit en Ctrl+Clic chaque élément. Puis ensuite clic droit, puis clic sur "Group"
-dissocier un groupe d'objet, dégrouper, en séléectionnant un groupe, puis clic droit pour ouvrir un menu puis clic gauche sur "De-group"
-ajouter des groupes d'objets ou objets paramétrés à notre toolbar en les déposant sur la toolbar en drag and drop (note, ces éléments ne doivent pas disparaître si l'utilisateur crée un nouveau document)
-undo / redo : ils s'appliquent sur les éléments suivants : l'ajout d'un élément ou groupe d'éléments sur le whiteboard, la suppression d'un élément de la toolbar, un groupage d'éléments, un groupage ou dégroupage, une édition des propriétés d'un élément, un ajout d'élément ou groupe d'élément à la toolbar, une suppression d'élément ou groupe d'élément du whiteboard. Undo nécessite qu'au moins une action undo-able ait été réalisée et n'ait pas été undone. Undo annule la dernière action undo-able en remettant le logciel dans son état précédant ladite action. Redo nécessite qu'au moins une action ait été undone, la dernière action undone est rejouée (par contre faut-il qu'il n'y ait pas eu d'action undo-able entre temps ? peut-on faire re-do plusieurs fois concernant le même undo ? est-ce qu'on considère que le dernier undo change, soit remplacé par le undo encore précédant une fois qu'il est re-done pour qu'on puisse refaire plusieurs suites de undo en faisant chaque undo à la suite ?)
-sauvegarder dans un document et choisir quel document charger (on ne sauvegarde que le whiteboard dans ces sauvegardes-ci)
-sauvegarde quand on quitte et reprendre cette sauvegarde au démarrage (il ne faut sauvegarder que la toolbar dans ce type de sauvegarde-là)
-justifier dans le rapport les patrons de conception
-davantage de tests
-diagramme d'architecture
-trash / delete pour retirer un élément de la toolbar (glissé-déposé de l'élément de la toolbar vers la poubelle), ou pour retirer un élément du whiteboard (glissé-déposé de l'élément ou groupe d'élément du whiteboard vers la poubelle)
-le livrable devra contenir les sources complètes (fichier zipped java, un .jar exécutable avec la commande java -jar monprojet.jar, un rapport détaillé, diagramme UML, explication des patterns (20 pages max), une vidéo avec commentaire de démonstration de l'application (4 minutes max)


DONE?:
-séparer les objets gérant le rendu graphique, des objets contenant les données de la scène




DONE:
-avoir un whiteboard
-déposer la figure sur le whiteboard (il faut le faire par drag and drop, par contre)
-sauvegarder un état, et charger de dernier état sauvegardé (mais il faut pouvoir choisir quelle sauvegarde charger)
-avoir des rectangles avec les propriétés : largeur, hauteur, rotation, position, centre de rotation, translation, couleur, arrondi des bords
-avoir des polygones réguliers avec les propriétés : nombre de côtés, longueur d'un côté, position, rotation, translation, couleur
-utiliser javaFX
-quelques tests